require_relative '../lib/input.rb'
require_relative '../lib/price_calculator.rb'
require_relative '../lib/bill.rb'

cart = Input.items
billed_cart = PriceCalculator.process(cart)
puts Bill.new(billed_cart).show
