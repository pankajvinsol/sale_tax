class Cart
  attr_accessor :item_list, :grand_total_of_items
  def initialize
    @item_list = []
    @grand_total_of_items = 0.00
  end
  def add_item(item)
    item_list.push(item)
  end
end
