require_relative 'item.rb'
class BilledItem < Item
  attr_reader :sale_tax, :import_tax, :total_price
  def initialize(item, sale_tax, import_tax, total_price)
    super(item.name, item.exempted_from_sale_tax, item.imported, item.price)
    @sale_tax = sale_tax
    @import_tax = import_tax
    @total_price = total_price
  end
end
