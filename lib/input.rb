require_relative 'item.rb'
require_relative 'cart.rb'
class Input
  N_REGEX = /^n$/i
  AMOUNT_REGEX = /^([0-9]{1,9})$|^([0-9]{1,9})(\.[0-9]{1,2})?$/
  def self.items
    cart = Cart.new
    continue = ""
    while not continue =~ N_REGEX
      print "Name of the product: "
      name = gets.chomp.squeeze
      imported = get_option('Imported? ', 'yes', 'no')
      exempted_from_sale_tax = get_option('Exempted from sales tax? ', 'yes', 'no')
      price = get_number('Price: ')
      continue = get_option('Do you want to add more items to your list(y/n): ', 'y', 'n')
      cart.add_item(Item.new(name, exempted_from_sale_tax, imported, price.to_f.round(2)))
    end
    cart
  end

  def self.get_option(input_message = '', *value_list)
    input = ''
    until value_list.include? input
      print input_message
      input = gets.chomp
    end
    input
  end

  def self.get_number(input_message = '')
    input = ''
    until input =~ AMOUNT_REGEX
      print input_message
      input = gets.chomp
    end
    input
  end
end
