require_relative 'billed_item.rb'
require_relative 'cart.rb'

class PriceCalculator
  SALE_TAX = 0.10
  IMPORT_TAX = 0.05
  def self.process(cart)
    cart.item_list = cart.item_list.map { |item| item_tax(item) }
    cart.grand_total_of_items = (cart_grand_total(cart)).round
    cart
  end
  def self.item_tax(item)
    sale_tax = item.exempted_from_sale_tax == 'yes' ? 0.00 : (SALE_TAX * item.price).round(2)
    import_tax = item.imported == 'yes' ? (IMPORT_TAX * item.price).round(2) : 0.00
    total_price = (item.price + sale_tax + import_tax).round(2)
    BilledItem.new(item, sale_tax, import_tax, total_price)
  end
  def self.cart_grand_total(cart)
    cart.item_list.inject(0.00) { |mem, billed_item| mem += billed_item.total_price }
  end
end
