class Item
  attr_reader :name, :exempted_from_sale_tax, :imported, :price
  def initialize(name, exempted_from_sale_tax, imported, price)
    @name = name
    @exempted_from_sale_tax = exempted_from_sale_tax
    @imported = imported
    @price = price
  end
end
