class Bill

  DASH_LINE = '-' * 79 + "\n"
  SEPARATOR = '|'

  def initialize(cart)
    @cart = cart
  end
  def show
    output = header
    @cart.item_list.each_with_index do |billed_item, index|
      output += generate_row(billed_item, index + 1)
    end
    output += footer
  end

  def header
    DASH_LINE + 'S.No.' + SEPARATOR + 'Item Name           ' + SEPARATOR + 'Imported Tax' + SEPARATOR + 'Sale Tax    ' + SEPARATOR + 'Price       ' + SEPARATOR + 'Total Price ' + "\n" + DASH_LINE
  end

  def footer
    footer_text = DASH_LINE
    footer_text += 'Grand Total' + ' ' * 54 + SEPARATOR + format_amount(@cart.grand_total_of_items, 12)  + "\n"
    footer_text += DASH_LINE
  end

  def generate_row(billed_item, index)
    row = ""
    row += format_amount(index, 5) + SEPARATOR
    row += format_string(billed_item.name, 20) + SEPARATOR
    row += format_amount(billed_item.import_tax, 12) + SEPARATOR
    row += format_amount(billed_item.sale_tax, 12) + SEPARATOR
    row += format_amount(billed_item.price, 12) + SEPARATOR
    row += format_amount(billed_item.total_price, 12) + "\n"
  end
  def format_string(string, length)
    string.to_s.length > length ? (string.to_s[0..length-4] + '...') : string.to_s.ljust(length)
  end

  def format_amount(amount, length)
    amount.to_s.rjust(length)
  end
end
